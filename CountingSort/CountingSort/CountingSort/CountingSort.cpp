// CountingSort.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
using namespace std;

class SortFactory{

public:
	SortBase createinstance(string sortType, vector<int> inputarray) {
		if (sortType == "merge") {

			return new MergeSort(inputarray);
		}
		else
		{
			return new CountingSort(inputarray);
		}

	}
}
class SortBase {
public:
	virtual void sort()=0;

};
class MergeSort: SortBase {
private:
	vector<int> _A;
	int _k;

public:
	

	MergeSort(vector<int> inputarray) {
		_A = inputarray;
		/*_B = B;*/
		_k = 9;


	}
	
	void sort() {}

};
class CountingSort: SortBase {
private:
	vector<int> _A;
	int _k;
public:
	vector<int> _B;

	CountingSort(vector<int> inputarray/*, vector<int> B*/) {
		_A = inputarray;
		/*_B = B;*/
		_k = 9;

		
	}
	void sort()
	{
		
		vector<int> C;
		for (int i = 0; i < _k+1; i++)
		{
			C.push_back(0);
		}
		for (int i = 0; i < _A.size()+1; i++)
		{
			_B.push_back(0);
		}
		
		for (int j = 0; j < _A.size(); j++)
		{
			int s = _A[j];
			C[s] = C[s] + 1;
		}
		for (int i = 1; i < _k+1; i++)
		{
			C[i] = C[i] + C[i - 1];
		}
		for (int j = _A.size()-1; j > -1; j--)
		{
			_B[C[_A[j]]] = _A[j];
			C[_A[j]] = C[_A[j]] - 1;
		}


	}
	void counting_sort(vector<int> A, vector<int> &B, int k)
	{

		vector<int> C;
		for (int i = 0; i < k + 1; i++)
		{
			C.push_back(0);
		}
		for (int i = 0; i < A.size() + 1; i++)
		{
			B.push_back(0);
		}

		for (int j = 0; j < A.size(); j++)
		{
			int s = A[j];
			C[s] = C[s] + 1;
		}
		for (int i = 1; i < k + 1; i++)
		{
			C[i] = C[i] + C[i - 1];
		}
		for (int j = A.size() - 1; j > -1; j--)
		{
			B[C[A[j]]] = A[j];
			C[A[j]] = C[A[j]] - 1;
		}


	}
};

int main()
{
	vector <int> inputArr = {1,5,2,3,7,4,4,9};
	/*vector<int> B;*/
	SortFactory sortfactory();
	CountingSort sortingClass(inputArr);
	MergeSort mergeSort(inputArr);
	sortingClass.sort();
	mergeSort.sort();
	/*sortingClass.counting_sort(inputArr, B, 9);*/
	for (int i = 0; i < sortingClass._B.size(); i++)
	{

		cout << sortingClass._B[i];
	}
	for (int x : sortingClass._B)
		cout << x << endl;
	return 0;
}

